package com.superdog.auth.common;


/**
 * 返回code
 *
 * @author ssliu
 */

public enum ResultCode {
    // 成功
    SUCCESS("0000", "success"),

    DATA_DUPLICATE("9998","用户已存在"),

    DATA_NOT_FOUND("9997","数据不存在"),

    GRAPH_NOT_READY("9996","类型图谱内容未准备好"),

    PARAM_BAD_FORMAT("9995","输入参数不规范"),

    SYSTEM_BUSY("9999","system busy");

    private String code;
    private String msg;

    ResultCode(String code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
