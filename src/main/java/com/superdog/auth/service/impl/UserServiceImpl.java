package com.superdog.auth.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.superdog.auth.common.constant.CommonConstant;
import com.superdog.auth.config.SystemInfoConfig;
import com.superdog.auth.exception.DataDuplicateException;
import com.superdog.auth.exception.DataNotFoundException;
import com.superdog.auth.po.User;
import com.superdog.auth.repository.UserRepository;
import com.superdog.auth.service.UserService;

/**
 * user 逻辑层实现类
 * @author ssliu
 */
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private SystemInfoConfig systemInfoConfig;
    public UserServiceImpl(UserRepository userRepository,
        SystemInfoConfig systemInfoConfig) {
        this.userRepository = userRepository;
        this.systemInfoConfig = systemInfoConfig;
    }

    @Override
    public String  findById(String userName,String systemId) {
        String result;
         Optional<User> byId = userRepository.findById(userName);
        if(!byId.isPresent()){
            //username not found
            result = "0";
            return result;
        }
        User user = byId.get();
        LocalDateTime expireTime = user.getExpireTime();
        String[] permission = user.getPermission().split(CommonConstant.PERMISSION_SEPARATOR);
        String pwd = user.getPassword();
        LocalDateTime now = LocalDateTime.now();
        if(now.isAfter(expireTime)){
            //expired
            result = "1";
        }else if(!Arrays.asList(permission).contains(systemId)){
            //no permission
            result = "2";
        }else{
            result = pwd;
        }
        return result;
    }

    @Override
    public Page<User> getAllUser(String userName, String systemCode, Pageable pageable) {
        if (ObjectUtils.isEmpty(userName)) {
            userName = null;
        }
        if (ObjectUtils.isEmpty(systemCode)) {
            systemCode = null;
        }
        Page<User> userPage = userRepository.findAll(userName, systemCode, pageable);
        List<User> userList = userPage.getContent();
        userList.parallelStream().forEach(user ->{
            convertPermission(user);
        });
        return userPage;
    }

    /**
     * 转换权限展示
     * @param user
     */
    private void convertPermission(User user) {
        String permission = user.getPermission();
        if (!ObjectUtils.isEmpty(permission)) {
            String[] permissionCodes = permission.split(CommonConstant.PERMISSION_SEPARATOR);
            List<String> permissionList = new ArrayList<>();
            for (String permissionCode : permissionCodes) {
                permissionList.add(systemInfoConfig.getSystemMap().get(permissionCode));
            }
            String permissions = String.join(CommonConstant.PERMISSION_SEPARATOR, permissionList);
            user.setPermissions(permissions);
        }
    }

    @Override
    public User getOneUser(String userName) {
        Optional<User> byId = userRepository.findById(userName);
        byId.ifPresent(this::convertPermission);
        return byId.orElse(null);
    }

    @Override
    public void addUser(User user, boolean isAdd) throws Exception {
        Optional<User> byId = userRepository.findById(user.getUserName());
        if (isAdd && byId.isPresent()) {
            throw new DataDuplicateException("用户已存在!");
        } else if (!isAdd && !byId.isPresent()) {
            throw new DataNotFoundException("用户不存在!");
        }
        LocalDateTime now = LocalDateTime.now();
        user.setCreateTime(now);
        userRepository.save(user);
    }

    @Override
    public void deleteUser(String userName) {
        Optional<User> byId = userRepository.findById(userName);
        if (byId.isPresent()) {
            userRepository.deleteById(userName);
        }
    }
}
