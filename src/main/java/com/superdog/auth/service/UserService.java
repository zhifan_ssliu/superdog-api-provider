package com.superdog.auth.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.superdog.auth.po.User;

/**
 * user service层
 * @author ssliu
 */
public interface UserService {
    /**
     * 通过用户名和系统ID，获得密码
     * @param userName 用户名
     * @param systemId 系统ID
     * @return 超级狗内置密码
     */
    String findById(String userName,String systemId);

    /**
     * 获取所有用户信息，支持根据用户名模糊查询，单个系统权限查询
     * @param userName
     * @param systemCode
     * @param pageable
     * @return
     */
    Page<User> getAllUser(String userName,String systemCode, Pageable pageable);

    /**
     * 获取指定用户
     * @param userName
     * @return
     */
    User getOneUser(String userName);

    /**
     * 新增用户
     * @param user
     * @param isAdd 是否是新增模式,否则是编辑模式
     * @throws Exception
     */
    void addUser(User user, boolean isAdd) throws Exception;

    /**
     * 删除用户
     * @param userName
     */
    void deleteUser(String userName);


}
