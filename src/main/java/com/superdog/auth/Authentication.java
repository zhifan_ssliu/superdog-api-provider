package com.superdog.auth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 调用超级狗动态库
 * 注意：因为包名涉及到调用本地库问题，该类需放在app同包下
 * @author ssliu
 */
@Component
public class Authentication {
    public static int status;
    private static Logger LOG = LoggerFactory.getLogger(Authentication.class.getName());

    static
    {
        try
        {
            if( System.getProperty("sun.arch.data.model").equals("64"))
            {

                System.loadLibrary( "dog_auth_srv_x64" );
            }
            else
            {
                System.loadLibrary( "dog_auth_srv" );
            }
        }
        catch(Exception e)
        {
            LOG.error("invoke dll error",e);
        }
    }

    public native static String getChallenge(int status[]);

    public native static int verifyDigest(int vendor, int dog, String challenge, String spn, String factor);

    public static String newChallenge()
    {
        int[] dll_status = {0};
        String s;
        s = getChallenge(dll_status);
        status = dll_status[0];
        return s;
    }




}
