package com.superdog.auth.config;

import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.superdog.auth.po.SystemInfoDTO;

import lombok.Data;

/**
 * 配置文件中的系统数据
 * @author ssliu
 */
@Configuration
@ConfigurationProperties(prefix = "app.system")
@Data
public class SystemInfoConfig {
  private Map<String,String> systemMap;
}
