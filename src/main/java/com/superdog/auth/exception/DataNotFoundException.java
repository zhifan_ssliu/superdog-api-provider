package com.superdog.auth.exception;

/**
 * @author ssliu
 * @date 2021-06-30
 */
public class DataNotFoundException extends Exception{
  public DataNotFoundException(String message) {
    super(message);
  }
}
