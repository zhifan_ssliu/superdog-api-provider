package com.superdog.auth.exception;

/**
 * @author ssliu
 * @date 2021-06-30
 */
public class DataDuplicateException extends Exception{
  public DataDuplicateException(String message) {
    super(message);
  }
}
