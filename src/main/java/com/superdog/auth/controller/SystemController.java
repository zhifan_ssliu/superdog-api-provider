package com.superdog.auth.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.superdog.auth.common.BaseResult;
import com.superdog.auth.config.SystemInfoConfig;

/**
 * @author ssliu
 * @date 2021-06-30
 */
@RestController
public class SystemController {

  private SystemInfoConfig systemInfoConfig;

  public SystemController(SystemInfoConfig systemInfoConfig) {
    this.systemInfoConfig = systemInfoConfig;
  }

  @GetMapping("/systems")
  public BaseResult getAllSystem() {
    return BaseResult.success(systemInfoConfig.getSystemMap());
  }





}
