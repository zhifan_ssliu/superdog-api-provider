package com.superdog.auth.controller;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.superdog.auth.common.BaseResult;
import com.superdog.auth.common.ResultCode;
import com.superdog.auth.exception.DataDuplicateException;
import com.superdog.auth.exception.DataNotFoundException;
import com.superdog.auth.po.User;
import com.superdog.auth.service.UserService;

/**
 * @author ssliu
 * @date 2021-06-30
 */
@RestController
public class UserController {

  private final UserService userService;

  public UserController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping("/users")
  public BaseResult getAllUser(String userName,String systemCode, Pageable page) {
    Page<User> allUser = userService.getAllUser(userName, systemCode, page);
    return BaseResult.success(allUser);
  }

  @GetMapping("/users/{userName}")
  public BaseResult getOneUser(@PathVariable String userName) {
    return BaseResult.success(userService.getOneUser(userName));
  }

  @PostMapping("/users")
  public BaseResult addUser(@RequestBody User user) {
    try {
      userService.addUser(user, true);
    } catch (DataDuplicateException e) {
      return BaseResult.error(ResultCode.DATA_DUPLICATE);
    } catch (Exception e) {
      return BaseResult.error(ResultCode.SYSTEM_BUSY);
    }
    return  BaseResult.success();
  }
  @PutMapping("/users")
  public BaseResult updateUser(@RequestBody User user) {
    try {
      userService.addUser(user, false);
    } catch (DataNotFoundException e) {
      return BaseResult.error(ResultCode.DATA_NOT_FOUND);
    } catch (Exception e) {
      return BaseResult.error(ResultCode.SYSTEM_BUSY);
    }
    return  BaseResult.success();
  }


  @DeleteMapping("/users/{userName}")
  public BaseResult deleteUser(@PathVariable String userName) {
    userService.deleteUser(userName);
    return BaseResult.success();
  }




}
