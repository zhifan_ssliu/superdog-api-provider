package com.superdog.auth.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ssliu
 * @date 2021-06-30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SystemInfoDTO {
  private String systemCode;
  private String systemName;
}
