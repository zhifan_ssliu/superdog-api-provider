package com.superdog.auth.po;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 超级狗内置用户实体类
 * @author ssliu
 */
@Entity
@AllArgsConstructor
@Data
@NoArgsConstructor
public class User implements Serializable {
    private static final long serialVersionUID = -1323641440478448251L;
    @Id
    private String userName;
    private String password;
    private String permission;
    @Transient
    private String permissions;
    @JsonFormat( pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime expireTime;
    @JsonFormat( pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

}
