package com.superdog.auth.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.superdog.auth.po.User;

/**
 * user dao层
 * @author ssliu
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {

  /**
   * 分页条件查询
   * @param userName
   * @param systemCode
   * @param pageable
   * @return
   */
  @Query(value = "select a from User a where (?1 is null or a.userName like %?1%) and (?2 is null or a.permission like %?2%)")
  Page<User> findAll(String userName,String systemCode, Pageable pageable);
}
