package com.superdog.auth.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.superdog.auth.Authentication;
import com.superdog.auth.service.UserService;

/**
 * 超级狗验证接口
 * @author ssliu
 */
@RestController
public class Auth {

    private Logger LOG = LoggerFactory.getLogger(Auth.class.getName());
    @Value("${dogAuth.vendor}")
    String vendor;
    @Value("${dogAuth.authCode}")
    String authCode;
    @Value("${dogAuth.factor}")
    String factor;

    private final UserService userService;
    public Auth(UserService userService) {
        this.userService = userService;
    }

    /**
     * 获取超级狗内置密码
     * @param request userName：超级狗内置用户名；systemId：应用系统ID
     * @param response 0：1：2：
     */
    @PostMapping("/getString")
    public void getString(HttpServletRequest request,HttpServletResponse response){
        String result = userService.findById(request.getParameter("userName"),request.getParameter("systemId"));
        JSONObject json = new JSONObject();
        json.put("result",result);
        try (PrintWriter pw = response.getWriter()){
            pw.write(json.toString());
            pw.flush();
        } catch (IOException e) {
            LOG.error("Get string failed!", e);
        }
    }


    @PostMapping("/getAuthCode")
    public void getAuthCode( HttpServletResponse response){
        String result = authCode;
        JSONObject json = new JSONObject();
        json.put("result",result);
        try (PrintWriter pw = response.getWriter()){
            pw.write(json.toString());
            pw.flush();
        } catch (IOException e) {
            LOG.error("Get authCode failed!",e);
        }
    }

    @PostMapping("/getChallenge")
    public void getChallenge(HttpServletRequest request, HttpServletResponse response){
        HttpSession session = request.getSession(true);
        String challenge = "";
        try{
            challenge = Authentication.newChallenge();
        }catch(Exception e){
            LOG.error("Get challenge failed!",e);
        }
        //set session
        session.setAttribute("LoginChallenge", challenge);
        LOG.debug("request sessionId ={}",request.getRequestedSessionId());
        LOG.debug("request challenge ={}",challenge);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("result",challenge);

        response.setHeader("Set-Cookie","JSESSIONID="+session.getId()+";HttpOnly;Secure;SameSite=None;");

        try(PrintWriter writer = response.getWriter()) {
            writer.write(jsonObject.toString());
            writer.flush();
        } catch (IOException e) {
            LOG.error("Write challenge failed!",e);
        }

    }

    @PostMapping("/doAuth")
    public void doAuth(HttpServletRequest request, HttpServletResponse response){
        //get params from page
         int iDogID = Integer.parseInt(request.getParameter("dogId"));
        String sResult = request.getParameter("digest");

        //get data from config
        int iVendorID = Integer.parseInt(vendor);
        String sFactor = factor;

        //Get challenge from session
        HttpSession session = request.getSession(true);

        Object sessionChallenge = session.getAttribute("LoginChallenge");
        String sChallenge = "";
        if(Objects.nonNull(sessionChallenge)){
            sChallenge = session.getAttribute("LoginChallenge").toString();
        }else{
            LOG.error("LoginChallenge property in session is null!");
        }

        //Set login session
        session.setAttribute("Login", "ON");

        int ret = Authentication.verifyDigest(iVendorID, iDogID, sChallenge, sResult, sFactor);
        String result;
        if(ret == 0)
            result =  "[00" + ret + "]";
        else
            result = "["+ret+"]";

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("result",result);
        try (PrintWriter writer = response.getWriter()){
            writer.write(jsonObject.toString());
            writer.flush();
        } catch (IOException e) {
            LOG.error("Write auth result failed!",e);
        }
    }

}
