package com.superdog.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 超级狗启动类
 * @author ssliu
 */
@SpringBootApplication
public class SuperdogApiProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SuperdogApiProviderApplication.class, args);
    }

}
